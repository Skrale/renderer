#include <iostream>
#include "tgaimage.h"

#define OUTPUT_FILE "output.tga"

const TGAColor black = TGAColor(  0,   0,   0, 255);
const TGAColor white = TGAColor(255, 255, 255, 255);
const TGAColor red   = TGAColor(255,   0,   0, 255);
const TGAColor green = TGAColor(  0, 255,   0, 255);
const TGAColor blue  = TGAColor(  0,   0, 255, 255);

int main() { 
	TGAImage image(100, 100, TGAImage::RGB);
	image.set(52, 41, red);
	image.flip_vertically(); // i want to have the origin at the left bottom corner of the image
	image.write_tga_file(OUTPUT_FILE);

	std::cout << "Successfuly!!!" << std::endl;
	return 0;
}

